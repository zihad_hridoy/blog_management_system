<div class="blog-masthead">
  <div class="container">
    <nav class="nav">
        <a class="nav-link active" href="{{ "/" }}">Home</a>
        <a class="nav-link" href="#">New features</a>
        <a class="nav-link" href="#">Press</a>
        <a class="nav-link" href="#">New hires</a>
        <a class="nav-link" href="#">About</a>
      

      <div class="row" style="margin-left: 30%; margin-top: 15px;">
        @if (!Auth::check())
          <form action="/login" method="get">
              <button type="submit" class="btn btn-info btn-sm" style=" color: #fff; border-color: #fff;">Login</button>
          </form>&nbsp; &nbsp;

          <form action="/register" method="get">
              <button type="submit" class="btn btn-info btn-sm" style=" color: #fff; border-color: #fff;">Register</button>
          </form>
        @endif
      </div>

      <div class="row">
        @if (Auth::check())
          <a class="nav-link ml-auto" href="/profile">{{ Auth::user()->name }}</a>
          <form action="/logout" method="get" accept-charset="utf-8">
              <button type="submit" class="btn btn-danger btn-sm" style=" color: #fff; border-color: #fff;  margin-top: 20%;">Logout</button>
          </form>
        @endif
    </div>

    </nav>
  </div>
</div>

<div class="blog-header">
  <div class="container">
    <h1 class="blog-title">The Blog with Laravel</h1>
    <p class="lead blog-description" style="margin-left: 1%;">An example blog template built with Laravel</p>
  </div>
</div>
