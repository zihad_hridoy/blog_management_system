@if (count($errors))
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li style="list-style-type: square;">{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif