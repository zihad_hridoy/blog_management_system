@extends ('layouts.master')

@section ('content')

		<div class="col-sm-8 blog-main">
		   <h1 style="text-align: center;">File Upload</h1><hr style="height: 2px; background: #428bca;">
	
		@include ('layouts.errors')

		<form action="{{ route('file.upload') }}" method="POST" enctype="multipart/form-data">
			
			{{ csrf_field() }}

			<div class="form-group">
				<label for="file_upload">Choose a File :</label>
				<input type="file" name="fileUpload" class="form-control" style="border: 1px solid #428bca;">
			</div>


			<div class="form-group">

				<button type="submot" class="btn btn-primary btn-block">Upload</button>
			</div>

		</form>
		

		<div class="row">
			<img src="{{ asset('/storage/up/a.jpg') }}" alt="img" style="width: 150px; height: 100px;">
		</div>
		</div>

@endsection