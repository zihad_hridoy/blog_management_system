@extends ('layouts.master')

@section ('content')

<div class="col-sm-8 blog-main">

	<div class="row">
		<div class="col-md-12">
			@if($flash = session('message'))
			  <div class="alert alert-success alert-dismissable" role="alert" style="text-align: center; font-weight: bold; color: #17a2b8;">{{ $flash }}</div>
			@endif
		</div>
	</div>
	@if (Auth::check())
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<img src="{{ Auth::user()->photo }}" alt="user_photo" style="height: 100%;width: 100%;">
				</div>
				<div class="col-md-4"></div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-2">Name :</div>
				<div class="col-md-5">{{ Auth::user()->name }}</div>
				<div class="col-md-2"></div>
			</div>


			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-2">Email :</div>
				<div class="col-md-5">{{ Auth::user()->email }}</div>
				<div class="col-md-2"></div>
			</div>
		@endif

		<div class="col-md-12" style="text-align: center;">
			<form action="/update_profile" method="get">
				<button type="submit" class="btn btn-info">Edit Post</button>
			</form>
		</div>
</div><!-- /.blog-main -->

@endsection