<div class="blog-post">
  
  <h2 class="blog-post-title">
	
	<a href="/posts/{{ $post->id }}">{{ $post->title }}</a>

  </h2>

  <p class="blog-post-meta">

  <a href="#">

    @if(!$post->user['id']) {{'anonymous'}}
    @else {{ $post->user['name'] }}
    @endif
    
  </a>


  	posted about
  	{{ $post->created_at->diffForHumans() }}
  </p>

   {{ $post->body }}
</div>
<!-- /.blog-post -->