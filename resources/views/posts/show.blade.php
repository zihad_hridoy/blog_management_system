@extends ('layouts.master')


@section ('content')
	<div class="col-sm-8 blog-main">
		<div class="row">
			<div class="col-md-12"><h1>{{ $post->title }}</h1></div>
		</div>
		

		<div class="row" style="margin-top: 1%;">
			<div class="col-md-12"><p>{{ $post->body }}</p></div>
		</div>

		<div class="comments" style="margin-bottom: 2%;">
			<ul class="list-group">
				
				@foreach ($post->comments as $comment)
					<li class="list-group-item">

					<span style="color: #03A9F4;">{{ $comment->created_at->diffForHumans() }} : &nbsp;</span>

					<a href="#">{{ $comment->user->name }}</a> :
					{{ $comment->body }}
					

					</li>
				@endforeach
				
			</ul>
		</div>
		
		@if(count($post->tags))
			
			<div class="card tags">
				<div class="card-block">
					<b>Tags:&nbsp;</b>
					
					@foreach($post->tags as $tag)
						<a href="/posts/tags/{{ $tag->name }}">
							<i class="fa fa-tags" aria-hidden="true">{{ $tag->name }}</i>&nbsp;&nbsp;
						</a>
					@endforeach
				</div>
			</div>
		@endif

				<div class="row" style="margin-bottom: 3%;">
						@if(Auth::user()->email==$post->user['email'])
						
							<div class="col-md-2">
								<form action="/posts/{{ $post->id }}/edit" method="get">
									<button type="submit" class="btn btn-info">Edit Post</button>
								</form>
							</div>

						@endif
					
				</div>

				<!--add tags-->
				@if(Auth::user()->email==$post->user['email'])
				
				@endif
				<!--end add tags-->


				<!--add comments-->

				<form method="POST" action="/posts/{{ $post->id }}/comments">
					
				  {{ csrf_field() }}

				  <div class="form-group">
				    <textarea name="body" class="form-control" style="border: 1px solid #007bff;" placeholder="your comment here..."></textarea>
				  </div>
				  
				  <div class="form-group">
				    <button type="submit" class="btn btn-primary">Publish</button>
				  </div>
				  

				</form>
	</div>
@endsection