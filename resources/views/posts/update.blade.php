@extends ('layouts.master')

@section ('content')

<div class="col-sm-8 blog-main">
	
	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	<form method="POST" action="/posts/{{ $post->id }}/update">

	  {{ csrf_field() }}
	  <input type="hidden" name="_method" value="PUT">

	  <div class="form-group">
	    <label for="title">Title:</label>
	    <input type="text" class="form-control" name="title" style="border: 1px solid #428bca;" value="{{ $post->title }}">
	  </div>

	  <div class="form-group">
	    <label for="body">Body:</label>
	    <textarea name="body" class="form-control" style="border: 1px solid #428bca; min-height: 200px;">{{ $post->body }}</textarea>
	  </div>

	  <button type="submit" class="btn btn-primary">Update</button>

	</form> 

</div><!-- /.blog-main -->

@endsection