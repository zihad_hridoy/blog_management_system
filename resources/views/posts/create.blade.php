@extends ('layouts.master')

@section ('content')

	<div class="col-sm-8 blog-main">
		<h1>Publish a Post</h1>

		<hr style="background:#428bca; height: 2px;"> 

		@include ('layouts.errors')

		<form method="POST" action="/posts">
			
		  {{ csrf_field() }}

		  <div class="form-group">
		    <label for="title">Title:</label>
		    <input type="text" class="form-control" name="title" style="border: 1px solid #428bca;">
		  </div>

		  <div class="form-group">
		    <label for="body">Body:</label>
		    <textarea name="body" class="form-control" style="border: 1px solid #428bca;"></textarea>
		  </div>

		  <button type="submit" class="btn btn-primary">Publish</button>

		</form>


	</div>

@endsection