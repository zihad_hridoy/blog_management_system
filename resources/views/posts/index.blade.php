@extends ('layouts.master')

@section ('content')

<div class="col-sm-8 blog-main">
	
	<div class="row" style="margin-bottom: 5%;">
	  <div class="col-sm-4">
	  	<form action="/posts/create">
	  	  <button type="submit" class="btn btn-info" style="">Add A New Blog</button>
	  	</form>
	  </div>

	  <div class="col-sm-8">
	    
	    @if($flash = session('message'))
	      <div class="alert alert-success alert-dismissable" style="text-align: center; font-weight: bold; color: #17a2b8;">
	      	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ $flash }}</div>
	    @endif

	  </div>

	</div>

  @foreach ($posts as $post)
    @include ('posts.post')
  @endforeach
  


</div><!-- /.blog-main -->

</script>
@endsection