@extends ('layouts.master')

@section ('content')

		<div class="col-sm-8 blog-main">
		   <h1 style="text-align: center;">Register</h1><hr style="height: 2px; background: #428bca;">
	
		@include ('layouts.errors')

		<form action="/register" method="POST">
			
			{{ csrf_field() }}

			<div class="form-group">
				<label for="name">Name :</label>
				<input type="text" name="name" class="form-control" style="border: 1px solid #428bca;">
			</div>

			<div class="form-group">
				<label for="email">Email :</label>
				<input type="Email" name="email" class="form-control" style="border: 1px solid #428bca;">
			</div>


			<div class="form-group">
				<label for="password">Password :</label>
				<input type="password" name="password" class="form-control" style="border: 1px solid #428bca;">
			</div>


			<div class="form-group">
				<label for="confirm_password">Confirm Password :</label>
				<input type="password" name="confirm_password" class="form-control" style="border: 1px solid #428bca;">
			</div>


			<div class="form-group">

				<button type="submot" class="btn btn-primary">Register</button>
			</div>

		</form>

		</div>

@endsection