@extends ('layouts.master')

@section ('content')

		<div class="col-sm-8 blog-main">
		   <h1 style="text-align: center;">Login</h1><hr style="height: 2px; background: #428bca;">
	
		@include ('layouts.errors')

		<form method="POST" action="/login">
			
		{{ csrf_field() }}

		 <div class="form-group">
			<label for="email">Email :</label>
			<input type="email" name="email" class="form-control" style="border: 1px solid #428bca;">
		 </div>


		 <div class="form-group">
		 	<label for="password">Password :</label>
			<input type="password" name="password" class="form-control" style="border: 1px solid #428bca;">
		 </div>	

			<div class="form-group">
				<button type="submot" class="btn btn-primary btn-block">Sign In</button>
			</div>

		</form>

		</div>

@endsection