<?php
namespace App;

use Carbon\Carbon;

class Post extends Model
{
   
   public function comments()
   {

   	return $this->hasMany(Comment::class);
   
   }


   public function user()
   {
   	return $this->belongsTo(User::class);
   }
   

   public static function archives()
   {
      return static::selectRAW('year(created_at) year,monthname(created_at)month,count(*)publish')
              ->groupBy('year','month')
              ->orderByRaw('min(created_at) desc')
              ->get()
              ->toArray();
   }


   public function tags()
   {

    //many to many relationship
    return $this->belongsToMany(Tag::class);
    
   }

}//end class
