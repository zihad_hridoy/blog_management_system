<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use Carbon\Carbon;

class PostsController extends Controller
{   

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {	
    	$posts = Post::latest();

        if ($month = request('month')) {
            $posts->whereMonth('created_at', Carbon::parse($month)->month);     
        }


        if ($year = request('year')) {
            $posts->whereYear('created_at', $year);     
        }

        $posts = $posts->get();


     ;

    	return view('posts.index', compact('posts', 'archives'));
    }

    public function create()
    {
    	return view('posts.create');
    }


    public function store()
    {

    	$this->validate(Request(),[

    		'title' => 'required',
    		'body' => 'required'

    		]);

    	Post::create([

            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->id()

        ]);


        session()->flash('message', 'your post has been added');

    	return redirect('/');
    }


    public function show(Post $post)
    {	
    	return view('posts.show', compact('post'));
    }


    public function edit(Post $post)
    {
        return view('posts.update', compact('post'));
    }    

  public function update(Request $request, $id)
  {     
      $post = Post::find($id);
      $post->title = $request->get('title');
      $post->body = $request->get('body');
      $post->save();

      session()->flash('message', 'Your Post Has Been Updated');
      return redirect('/');
  }
}
