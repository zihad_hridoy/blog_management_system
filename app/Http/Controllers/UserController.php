<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;

class UserController extends Controller
{
    public function index()
    {
    	return view('user.profile');
    }


	public function edit()
	{
		return view('user.update_profile');
	}

	public function update(Request $request, $id)
	{     
	    $user = User::find($id);
	    $user->name = $Request->get('name');
	    $user->email = $Request->get('email');
	    //$user->image = $Request->get('image');
	    $user->save();

	    session()->flash('message', 'Your Profile Has Been Updated');
	    return redirect('/');
	}
    	

}
