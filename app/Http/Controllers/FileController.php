<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\File;

class FileController extends Controller
{
    public function index()
    {
    	return view('file.upload');
    	
    }

    public function store(Request $req)
    {
    	if ($req->hasFile('fileUpload')) {

    		$originalName = $req->fileUpload->getClientOriginalName();
    		$fileSize = $req->fileUpload->getClientSize();

    		$req->fileUpload->storeAs('public/up', $originalName);

    		$file = new File();

    		$file->name = $originalName;
    		$file->size = $fileSize;
    		$file->save();
    		return view('file.upload');
    	}
    }
}
