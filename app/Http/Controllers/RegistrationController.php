<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Mail\welcome;

class RegistrationController extends Controller
{
    public function create()
    {
    	return view('registration.create');
    }

    public function store()
    {	

    	//validate
    	$this->validate(Request(),[

    		'name' => 'required',
    		'email' => 'required|email',
    		'password' => 'required',
            'confirm_password' => 'required|same:password'
    	]);

    	//create and save user
    	$user = User::create(request(['name', 'email', 'password']));

    	//sign them in 
    	auth()->login($user);

        //send welcome email

        \Mail::to($user)->send(new welcome($user));

    	return redirect()->home();
    }

}
